//converters between numbers in the Roman and Arabic systems

object RomanNumbers extends App {
  def arabToRom(r: Int, lista: List[(String, Int)]): String = {
    if (lista.nonEmpty) {
      lista.head._1 * (r / lista.head._2) + arabToRom(r % lista.head._2, lista.tail)
    } else ""
  }

  def romToArab(s: String, lista: List[(String, Int)]): Int = {
    if (lista.nonEmpty) {
      if (s.startsWith(lista.head._1))
        lista.head._2 + romToArab(s.drop(lista.head._1.length), lista)
      else
        romToArab(s, lista.tail)
    } else 0
  }

  val arab_romList = List(("M", 1000), ("CM", 900), ("D", 500), ("CD", 400), ("C", 100), ("XC", 90),
    ("L", 50), ("XL", 40), ("X", 10), ("IX", 9), ("V", 5), ("IV", 4), ("I", 1))

  def testArabToRom(in:Int): Unit = println(in+": "+arabToRom(in,arab_romList))
  def testRomToArab(in:String): Unit = println(in+": "+romToArab(in,arab_romList))

  testArabToRom(2018)
  testArabToRom(999)
  testArabToRom(1997)

  testRomToArab("MMXVIII")
  testRomToArab("CMXCIX")
  testRomToArab("MCMXCVII")


}
