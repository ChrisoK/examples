//airplanes currently in the air within a radius of 50 km from SoftServe in Wroclaw

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.log4j.{Level, Logger}
import play.api.libs.json._
import org.apache.spark.sql.functions._
import scala.io.Source

object Airplanes extends App {
  val rootLogger = Logger.getRootLogger
  rootLogger.setLevel(Level.ERROR)
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  Logger.getLogger("kafka").setLevel(Level.OFF)
  Logger.getLogger("spark").setLevel(Level.OFF)
  Logger.getLogger("yarn.Client").setLevel(Level.OFF)

  val distanc = udf((lat_2: Double, long_2: Double) => {
    val AVERAGE_RADIUS_OF_EARTH_KM = 6371
    val lat_nad, lat_1 = 52.229770
    val lon_nad, long_1 = 21.011780

    val latDistance = Math.toRadians(lat_2 - lat_1)
    val lngDistance = Math.toRadians(long_2 - long_1)
    val sinLat = Math.sin(latDistance / 2)
    val sinLng = Math.sin(lngDistance / 2)
    val a = sinLat * sinLat +
      (Math.cos(Math.toRadians(lat_2))
        * Math.cos(Math.toRadians(lat_1))
        * sinLng * sinLng)
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    AVERAGE_RADIUS_OF_EARTH_KM * c
  }
  )
  val getCityName = udf((in: String) => in match {
    case null => None
    case s => Some(s.drop(5))
  })

  val config = new SparkConf()
    .setAppName("airplanes")
    .setMaster("local[4]")
  val sc = new SparkContext(config)
  val sqlContext = new SQLContext(sc)

  val url_base = "https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?lat="
  val lat = 52.229770
  val long = 21.011780
  val radius: String = "50"

  val url = url_base + lat.toString + "&lng=" + long.toString + "&fDstL=0&fDstU=" + radius
  System.setProperty("http.agent", "Chrome")
  val json_raw = Source.fromURL(url).mkString
  val json = Json.parse(json_raw)
  val airplanes = (json \ "acList").get.toString
  val airplanesRdd = sc.parallelize(Seq(airplanes))

  import sqlContext.implicits._
  val airplanesDF = sqlContext.read.json(airplanesRdd)

  val columnListRequired = List("Man", "Type", "Year", "Op", "From", "To", "Gnd", "Lat", "Long")
  val columnlistInDF = airplanesDF.columns.toList
  if (columnListRequired.forall(columnlistInDF.contains)) {
    airplanesDF.filter(!$"Gnd")
      .withColumn("Distance", round(distanc($"Lat", $"Long"), 2))
      .withColumn("newFrom", getCityName($"From")).withColumn("newTo", getCityName($"To"))
      .select("Man", "Type", "Year", "Op", "newFrom", "newTo", "Distance").orderBy($"Distance").show
  }
  else
    println("empty")

}
